package org.hansed.moduleColor.notification;

import com.intellij.openapi.components.ServiceManager;
import com.intellij.ui.EditorNotificationPanel;
import org.hansed.moduleColor.components.ColorStorage;

import java.awt.*;

/**
 * @author HanSed
 * <p>
 * ColoredNotificationPanel, uses the {@link ColorStorage} service to color its background
 */
class ColoredNotificationPanel extends EditorNotificationPanel {

    /**
     * @param text   text to be displayed on the notification
     * @param module name of the module this notification is referencing
     */
    ColoredNotificationPanel(String text, String module) {
        setText(text);
        myBackgroundColor = ServiceManager.getService(ColorStorage.class).getColor(module);
        createActionLabel("Change color", () -> {
            ColorChooser.create(module, myBackgroundColor);
        });

    }


    void setColor(Color color) {
        this.myBackgroundColor = color;
    }
}
