package org.hansed.moduleColor.notification;

import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.fileEditor.FileEditor;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.roots.ProjectFileIndex;
import com.intellij.openapi.util.Key;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.ui.EditorNotificationPanel;
import com.intellij.ui.EditorNotifications;
import org.hansed.moduleColor.components.ColorStorage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author HanSed
 */
public class ColorNotification extends EditorNotifications.Provider<EditorNotificationPanel> {

    private static final Key<EditorNotificationPanel> KEY = Key.create("ColorStorage");

    private static HashMap<String, ArrayList<ColoredNotificationPanel>> panels = new HashMap<>();

    /**
     * Refresh all panels of the files of a specific module
     *
     * @param moduleName name of the module
     */
    static void refreshPanel(String moduleName) {
        if (panels.containsKey(moduleName)) {
            Color newColor = ServiceManager.getService(ColorStorage.class).getColor(moduleName);
            for (ColoredNotificationPanel panel : panels.get(moduleName)) {
                panel.setColor(newColor);
                panel.repaint();
            }
        }
    }

    @NotNull
    @Override
    public Key<EditorNotificationPanel> getKey() {
        return KEY;
    }

    /**
     * Create a new notification panel containing information about the opened file
     */
    @Nullable
    @Override
    public EditorNotificationPanel createNotificationPanel(@NotNull VirtualFile virtualFile, @NotNull FileEditor fileEditor) {
        Project[] projects = ProjectManager.getInstance().getOpenProjects();
        String moduleName = "outside module";
        Module module = null;
        for (Project project : projects) {
            module = ProjectFileIndex.getInstance(project).getModuleForFile(virtualFile);
            if (module == null)
                continue;
            break;
        }
        if (module != null) {
            moduleName = module.getName();
        }
        panels.putIfAbsent(moduleName, new ArrayList<>());
        ColoredNotificationPanel panel = new ColoredNotificationPanel(virtualFile.getName() + " " + (moduleName.equals("outside module") ? moduleName : "in module: " + moduleName), moduleName);
        panels.get(moduleName).add(panel);
        return panel;
    }
}
