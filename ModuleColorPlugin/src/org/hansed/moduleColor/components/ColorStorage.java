package org.hansed.moduleColor.components;

import com.intellij.openapi.components.*;
import com.intellij.util.xmlb.XmlSerializerUtil;
import com.intellij.util.xmlb.annotations.MapAnnotation;
import org.jetbrains.annotations.Nullable;

import java.awt.*;
import java.util.HashMap;
import java.util.Random;


/**
 * @author HanSed
 */
@State(
        name = "ColorStorage",
        storages = {
                @Storage("moduleColor.xml")
        }
)

public class ColorStorage implements PersistentStateComponent<ColorStorage> {

    @MapAnnotation
    private HashMap<String, Integer> reds;
    @MapAnnotation
    private HashMap<String, Integer> blues;
    @MapAnnotation
    private HashMap<String, Integer> greens;

    ColorStorage() {
        reds = new HashMap<>();
        blues = new HashMap<>();
        greens = new HashMap<>();
    }

    /**
     * Set the color of a certain module name
     *
     * @param key   name
     * @param color new color
     */
    public void setColor(String key, Color color) {
        reds.put(key, color.getRed());
        greens.put(key, color.getGreen());
        blues.put(key, color.getBlue());
    }

    /**
     * Get a color for specified module name
     *
     * @param module the name of the module
     * @return color specified earlier or a newly generated one if none is present
     */
    public Color getColor(String module) {
        if (reds.containsKey(module)) {
            return new Color(reds.get(module), greens.get(module), blues.get(module));
        }
        Random random = new Random(module.hashCode());
        float r = random.nextFloat();
        float g = random.nextFloat();
        float b = random.nextFloat();

        Color randomColor = new Color(r, g, b);
        setColor(module, randomColor);
        return randomColor;
    }

    @Nullable
    @Override
    public ColorStorage getState() {
        return this;
    }

    @Override
    public void loadState(ColorStorage state) {
        XmlSerializerUtil.copyBean(state, this);
    }
}
